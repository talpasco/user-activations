# 1) Pico - Facebook & Twilio webhooks

		## Requirements

		* Lumen 6.0
		* PHP 8
		* Composer
		* Twilio Account
		* Facebook page + developer account
		* Ngrok

		## Get Code

		* Clone or download this repository (https://bitbucket.org/talpasco/user-activations)
		* `git clone https://bitbucket.org/talpasco/user-activations` 
		* `cd user-activations/pico-webhooks` - move into directory just created
		* run: php artisan migrate (Migrate DB scheme)
		* edit .env file
		* run Makefile
		* ngrok http 9000

		## Navigation

		* Routing is implemented within https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/routes/web.php.
		* https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Http/Controllers/Controller.php - Implements the Payload class and the functionality of storing/sending data.
		* https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Http/Controllers/FaceBookController.php - verification & handling FaceBook messages (inherits controller.php).
		* https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Http/Controllers/TwilioController.php - verification & handling Twilio messages (inherits controller.php).
		* DB migrations - https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/database/migrations/2021_07_25_120750_create_webhooks_table.php .
		* Added Event propagation (via web-socket) functionality (Placeholder & inheritance) -
		https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Events/MessageSent.php ,
		https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Listeners/MessageSentConfirmation.php ,
		https://bitbucket.org/talpasco/user-activations/src/master/pico-webhooks/app/Providers/EventServiceProvider.php .

		## TODOS:

		* Implement "parseResponse" function
		* WS Implementation
		* Session-store and PubSub
		* Unit Tests.

# 2) Pico - SQL
		## The dump files (including Routines & Create Scheme) are located at:
			user-activations\SQL
			
		## 1st Task
			CREATE DEFINER=`root`@`localhost` PROCEDURE `UserData_List`()
			BEGIN

			SELECT * FROM table_a JOIN table_b
			ON table_a.user_id = table_b.user_id;

			END

		## 2nd Task
			CREATE DEFINER=`root`@`localhost` PROCEDURE `UserData_GetByActivationId`(
			IN activitionID INT
			)
			BEGIN

			SELECT A.user_id, A.social_network_id,
			D.activation_id,
			GROUP_CONCAT(DISTINCT B1.value) as first_name,
			GROUP_CONCAT(DISTINCT B2.value) as last_name, 
			GROUP_CONCAT(DISTINCT B3.value) as gender,
			GROUP_CONCAT(DISTINCT B4.value) as email,
			GROUP_CONCAT(DISTINCT B5.value) as phone_number,
			COUNT(DISTINCT D.widget_id) AS number_of_widgets
			FROM table_a AS A
			JOIN table_b AS B ON A.user_id = B.user_id
			JOIN table_c AS C ON B.data_id = C.data_id
			LEFT JOIN table_b B1 ON C.name = "First name" AND B1.data_id = C.data_id AND B1.user_id = B.user_id
			LEFT JOIN table_b B2 ON C.name = "Last name" AND B2.data_id = C.data_id AND B2.user_id = B.user_id
			LEFT JOIN table_b B3 ON C.name = "Gender" AND B3.data_id = C.data_id AND B3.user_id = B.user_id
			LEFT JOIN table_b B4 ON C.name = "Email" AND B4.data_id = C.data_id AND B4.user_id = B.user_id
			LEFT JOIN table_b B5 ON C.name = "Phone Number" AND B5.data_id = C.data_id AND B5.user_id = B.user_id
			JOIN table_d AS D ON A.user_id = D.user_id
			WHERE D.activation_id = activitionID
			GROUP BY A.user_id;

			END
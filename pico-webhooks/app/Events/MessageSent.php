<?php

namespace App\Events;

use App\Providers\EventServiceProvider;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class MessageSent extends Event
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @param  Message  $message
     * @return void
     */
    public function __construct(EventServiceProvider $message)
    {
        $this->message = $message;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FaceBookController extends Controller
{
    /**
     * The verification token for Facebook
     *
     * @var string
     */
    //protected $token;

    public function __construct()
    {
        $this->token = env('BOT_VERIFY_TOKEN');
    }

    /**
     * Verify the token from Messenger. This helps verify your bot.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function verify_token(Request $request)
    {
        $mode  = $request->get('hub_mode');
        $token = $request->get('hub_verify_token');

        if ($mode === "subscribe" && $this->token && $token === $this->token) {
            return response($request->get('hub_challenge'), 200);
        }

        return response("Invalid token!", 400);
    }

    /**
     * Handle the query sent to the bot.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function handle_query(Request $request)
    {
        $entry = $request->get('entry');
        $payload = new Payload();
        $payload->userId = \array_get($entry, '0.messaging.0.sender.id'); //Parse message
        $payload->pageId = \array_get($entry, '0.messaging.0.recipient.id');
        $payload->message  = \array_get($entry, '0.messaging.0.message.text');
        $msg = $this->storeMessage($payload); //Serialize object and store in DB and Cache. Returns Serialized object as string;
        $this->propagateMsg($payload->userId, $msg); //Publish the Serialized object based on the topic (userId);
        return response($payload->message, 200);
    }
}

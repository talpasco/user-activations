<?php

namespace App\Http\Controllers;

use App\Twilio;
use App\Webhook;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class TwilioController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());
        $payload = new Payload();
        $payload->userId = $request->get('userId'); //Parse message
        $payload->message  = $request->get('message');
        $msg = $this->storeMessage($payload); //Serialize object and store in DB and Cache. Returns Serialized object as string;
        $this->propagateMsg($payload->userId, $msg); //Publish the Serialized object based on the topic (userId);

        $url = config('app.url');// . "/webhook/{$webhook->identifier}";

        $result = [
            'message' => "Webhook has been created successfully",
            'data' => "Webhook URL is {$url}"
        ];

        return response()->json($result);
    }

    public function dispatchNotification($webhook, Twilio $twilio)
    {
        $webhook = Webhook::whereIdentifier($webhook)->firstOrFail();

        $twilio->notify(env('TWILIO_SMS_TO'), $webhook->message);

        $result = [
            'message' => 'Message has been delivered'
        ];
        $res = response()->json($result);
        return  $res;
    }

    protected function rules()
    {
        return [
            'userId' => 'required',
            'message' => 'required'
        ];
    }
}

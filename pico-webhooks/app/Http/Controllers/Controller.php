<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use App\Webhook;

use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Hash;

use App\Events;
use App\Providers\EventServiceProvider;
use App\Events\MessageSent;
use App\Listeners\MessageSentConfirmation;

class Payload
{
    public $userId;
    public $message;
    public $pageId;
}

class Controller extends BaseController
{
    /**
     * Propagate message via web-socket.
     *
     * @param  string $userId
     * @param  string $serializedMsg
     * @return string
     */
    protected function propagateMsg($userId, $serializedMsg)
    {
        // $broadcastMsg = MessageSent::findOrFail($userId);

        // Event::fire(new MessageSentConfirmation($serializedMsg));

        // return $broadcastMsg;
        // TODO: Use Redis as a PubSub mechanism to propagate messages to the user (userId as the topic)
    }

    /**
     * Cache the messanger message and/or store it to the DB.
     *
     * @param  class $payload
     * @return string
     */
    protected function storeMessage($payload)
    {
        $serializedObj = serialize($payload);
        // md5 func to set a uniqe key to the cache entry,
        // whether it's needed to store it to the DB, or it's already been stored
        $payloadKey = md5($serializedObj);
        $cachedPayload = Cache::get($payloadKey);
        if (!$cachedPayload) {
            //Store in DB if not already cached
            Webhook::create([
                'userId' => $payload->userId,
                'pageId' => $payload->pageId,
                'message' => $payload->message
            ]);
            // Cache the object
            Cache::forever($payloadKey, $serializedObj);
            return Cache::get($payloadKey);
        } else {
            return $cachedPayload;
        }
    }
}

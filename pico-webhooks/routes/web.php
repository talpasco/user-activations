<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/webhook/chat', 'FaceBookController@verify_token');
$router->post('/webhook/chat', 'FaceBookController@handle_query');
$router->post('/webhook/message', 'TwilioController@store');
$router->post('/webhook/message', 'TwilioController@dispatchNotification');